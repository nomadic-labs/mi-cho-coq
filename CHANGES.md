



Version 1.0.0
=============

Build
-----

- Added support for Coq version 8.11

- Dropped support for Coq versions strictly older than 8.10

Mi-Cho-Coq framework
--------------------

- Removed most functors to simplify instantiating the framework.

- Formalized annotations and entrypoints.

- Formalized timestamps using the coq-moment library.

- Formalized byte sequences with conversion functions with hexadecimal strings.

- Instructions have been stratified as follows:
  - opcodes,
  - instructions,
  - instruction sequences.

  Opcodes are the instructions that do not take subprograms as
  argument nor have special treatment in the type-checker or
  evaluator. Most of the instructions in Michelson fall into this
  category, putting them aside makes the number of constructors of the
  instruction ASTs much smaller which is needed when reasoning on the
  syntax.

- Added a friendly notations to write Michelson scripts using a syntax
  very close to the usual syntax of Michelson.

- Formalized typing and untyping modes. Michelson data expressions can
  be written in two modes called "optimized" and "readable". The OCaml
  Michelson typechecker accepts both modes (and even expressions
  mixing both modes) and the Michelson pretty-printer can be
  configured to output expressions in either mode. Mi-Cho-Coq's
  untyper can similarly be configured to use either mode just
  like. Mi-Cho-Coq's typechecker can be configured to accept
  expressions in optimized mode, readable mode, or both. In the
  optimized case, injectivity of type elaboration has been proved.

- Introduced and certified a Michelson optimizer.

- Added lemmas `precond_iter` and `precond_iter_bounded` to help
  reasoning on the `ITER` instruction.

- Formalized the relationship between key hashes, addresses, and
  contracts. Key hashes and contract addresses are still axiomatized
  though.

- Simplified the formula produced by the weakest-precondition
  calculus (the `eval_precond` function).

- Changed the representations of sets and maps to enforce the sorting
  invariant statically.

- Changed the representation of mutez to use the builtin int63 type
  introduced in Coq v8.10.

- Formalized the Michelson type classes: `comparable`, `pushable`,
  `passable`, `storable`, `packable`, and `big_map_value`.

- Formalized operations with an inductive type.

- Generalized the `map.remove_present_untouched` lemma.

- Redefined `set.remove` so that it computes.

Coq-of-OCaml
------------

- Proved one side of the equivalence between the representation of
  types in Mi-Cho-Coq and the one translated from the Tezos code base
  using coq-of-ocaml.

- Proved both sides of the equivalence between the representation of
  comparable types.

Smart contract verification
---------------------------

- Removed duplicated proof of the boomerang contract.

- Verified [the spending limit contract](https://blog.nomadic-labs.com/formally-verifying-a-critical-smart-contract.html).

- Verified a vesting contract.

Performance
-----------

- The parser is now called only once in the main file.

- Defined a fuel-free version of the evaluator restricted to a
  terminating fragment of Michelson (without loops nor lambdas).

Bug fixes
---------

- Type-checking a mutez literal now fails in case of overflow.

- The `int : nat : 'S  ->  nat : 'S` case for the `AND` instruction was missing, it has been added.

- The `chain_id` and `big_map` cases were missing in the parser for Michelson data. They have been added.

- The semantics of the `EDIV` instruction was sometimes wrong when `EDIV` was used on negative numbers. This has been fixed.

- Allowed `FAILWITH` in loop instructions `LOOP`, `LOOP_LEFT`, and `ITER`.

- The `False` constant is not shadowed in util.v anymore.



Version 0.1
===========

First public release of Mi-Cho-Coq
